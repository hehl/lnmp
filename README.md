# 欢迎使用 LNMP一键部署脚本
---
***本项目已停止开发！因长时间未对代码进行维护，可能会造成项目在不同环境上无法部署、运行BUG等问题，请知晓！项目仅供参考！***

因本人工作原因，此项目以后不提供更新及维护，项目仅供参考。

**一.简介**
LNMP一键部署脚本是一键部署web环境所用，非常适合网站运维人员，web开发人员在Linux系统中部署web环境.

您可以使用LNMP一键部署脚本：
> * 自动部署Nginx,Mysql,Php
> * 备份数据库
> * 升级软件版本
> * 添加虚拟站点和目录  

**支持系统:**  
CentOS 6.x  
CentOS 7.x  
Ubuntu 14.x  
Ubuntu 16.x  

**软件版本**
```  
nginx-1.12.0
php-5.6.40
php-7.3.21
mysql-5.6.49
mysql-5.7.31
redis-4.0.0
phpredis redis-3.1.4

```  

**FAQ专区**  
如果mysql,php下载地址失效,可以自主修改include/common.sh内的版本号即可

**正式版下载**  
https://gitee.com/hehl/lnmp/repository/archive/v1.0.2.zip

**最新版**  
```
git clone https://gitee.com/hehl/lnmp.git  
```  
**安装截图**  
![avatar](src/i1.gif)
![avatar](src/i2.gif)

**二.文件目录**

```
conf                                   #软件配置文件夹
include                                #系统软件部署脚本文件夹
init.d                                 #服务脚本文件夹
src                                    #软件包文件夹
db_backup_import.sh                    #数据导出和导入脚本
ftp_manage                             #pureftp管理脚本
install.sh                             #LNMP一键部署脚本
uninstall.sh                           #一键卸载脚本
upgrade.sh                             #一键升级脚本
vhost.sh                               #一键增加Nginx虚拟站点和网站目录
php_swich.sh                           #一键切换Php版本
reset_mysql_pwd.sh                     #一键重置Mysql账号root的密码

```


### 更新记录  
201809
恢复安装截图

201711
增加一条命令直接安装lnmp，无需交互式询问

201709
增加MySQL数据库密码一键重置脚本;
增加php版本一键切换,支持最新版本7.1.9,自动安装指定版本,如已经安装,再次来回切换无需重新安装

201708
增加ftp软件pureftp的安装部署

201707
增加支持ubuntu16
增加redis部署和管理

201706
增加支持ubuntu14

201705
开始写这个脚本

### 注:
****
1.建议安装git克隆项目,方便以后拉取最新版本  
2.Mysql百度盘下载链接：http://pan.baidu.com/s/1qY2ZJB2 密码：ea5x  
3.有任何问题和建议[点我](https://gitee.com/hehl/lnmp/issues)提交  
4.脚本运行中如果无法退出，请按Ctrl+c组合键退出

